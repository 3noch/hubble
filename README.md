hubble
======

A web-based visualization tool for diagramming a code-base or product and
managing change.


References
==========

 * Directed Graph Editor: http://bl.ocks.org/rkirsling/5001347
 * Sticky Force Layout: http://bl.ocks.org/mbostock/3750558
