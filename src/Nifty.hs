module Nifty where

import Control.Monad.IO.Class (MonadIO, liftIO)

(=:) :: a -> b -> (a, b)
(=:) = (,)

infixr 1 <?
(<?) :: (a -> Bool) -> [a] -> [a]
(<?) = filter

(<$>>) :: (Functor f1, Functor f) => (a -> b) -> f (f1 a) -> f (f1 b)
(<$>>) = fmap fmap fmap

io :: MonadIO m => IO a -> m a
io = liftIO
