module Hubble.Graph.Query where

import Data.Graph.Inductive (Gr, Node, Edge, LNode, LEdge, mkGraph, lpre, lsuc, nodes, empty, ufold, labNodes, gelem, (&))
import Data.List (intersect, nub, delete)

import Hubble.Graph.Query.Lang
import Hubble.Graph.Types
import Nifty

a `contains_` b = To (Label "contains") (DepthOf 1) a b

testExpr = EmptyExpr `contains_` WildNode

testGr :: HubGraph
testGr = mkGraph
  [ 1 =: "a"
  , 2 =: "b"
  , 3 =: "c"
  ]
  [ (1, 2, "contains")
  , (2, 1, "contains")
  , (3, 9, "is")
  ]


edgeFilter :: EdgeLabel -> String -> Bool
edgeFilter (Label edge) = (edge ==)
edgeFilter WildEdge     = const True


eval :: Expr -> HubGraph -> [Node]
eval EmptyExpr       _  = []
eval (Capture a)     gr = eval a gr
eval WildNode        gr = nodes gr
eval (NodeName name) gr = [n | (n, nName) <- labNodes gr, nName == name]
eval (NodeId n)      gr = if gelem n gr then [n] else []
eval (To edgeLabel (DepthOf 1) a' b') gr = nub $ matching (eval a' gr) (eval b' gr)
  where
    edgeMatches = edgeFilter edgeLabel

    matching :: [Node] -> [Node] -> [Node]
    matching [] [] = []
    matching [] bs = null.froms <? bs
    matching as [] = null.tos   <? as
    matching as bs = concat [ a:bs' | a <- as
                            , let bs' = intersect (tos a) bs
                            , not.null $ bs' ]

    tos   = withEdge . lsuc gr
    froms = withEdge . lpre gr

    withEdge :: [(Node, String)] -> [Node]
    withEdge d = [n | (n, e) <- d, edgeMatches e]


close :: [Node] -> HubGraph -> HubGraph
close [] _ = empty
close initNodes gr = snd $ ufold f (initNodes, empty) gr
  where
    (froms, thisNode, name, tos) `f` (nodesLeft, newGr)
      | thisNode `elem` nodesLeft = (delete thisNode nodesLeft, newCtx & newGr)
      | otherwise                 = (nodesLeft, newGr)
      where newCtx = ( (`elem` initNodes).snd <? froms
                     , thisNode
                     , name
                     , (`elem` initNodes).snd <? tos
                     )
