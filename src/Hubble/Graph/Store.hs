{-# LANGUAGE OverloadedStrings, FlexibleInstances #-}

module Hubble.Graph.Store
  ( store
  , load
  , asBz
  , squish
  ) where

import Control.Applicative
import Control.Monad (mzero)
import Data.Aeson
import Data.Aeson.Types (Parser)
import qualified Data.ByteString.Lazy as Bz
import Data.Graph.Inductive as Gr (Graph, DynGraph, Node, LNode, LEdge, mkGraph, empty, labNodes, labEdges, ufold, (&))
import Data.Maybe (fromMaybe, fromJust)

import Hubble.Graph.Types
import Nifty

at map_ = fromJust . (flip lookup) map_

squish :: (Graph gr1, DynGraph gr2) => gr1 a b -> gr2 a b
squish = snd . ufold f ((0, []), Gr.empty)
  where
    (froms, oldId, name, tos) `f` ((newId, nodeMap), newGr) = (newState, newCtx & newGr)
      where
        newCtx     = (remap froms, newId, name, remap tos)
        remap      = map (\(edge, node) -> (edge, newNodeMap `at` node))
        newNodeMap = (oldId, newId):nodeMap
        newState   = (newId+1, newNodeMap)


instance ToJSON HubGraph where
  toJSON gr = object [ "nodes" .= (jsonNode <$> labNodes gr)
                     , "links" .= (jsonEdge <$> labEdges gr) ]
    where
      jsonNode :: (Node, String) -> Value
      jsonNode (n, name)    = object ["id" .= n, "name" .= name]

      jsonEdge :: (Node, Node, String) -> Value
      jsonEdge (a, b, name) = object ["source" .= a, "target" .= b, "name" .= name]


instance FromJSON HubGraph where
  parseJSON (Object v) = mkGraph
      <$> (v .: "nodes" >>= mapM jsonNode)
      <*> (v .: "links" >>= mapM jsonEdge)
    where
      jsonNode :: Value -> Parser (LNode NodeName)
      jsonNode (Object v) = (,) <$> v .: "id" <*> v .: "name"
      jsonNode _          = mzero

      jsonEdge :: Value -> Parser (LEdge EdgeName)
      jsonEdge (Object v) = (,,) <$> v .: "source" <*> v .: "target" <*> v .: "name"
      jsonEdge _          = mzero

  parseJSON _ = mzero


store :: HubGraph -> IO ()
store = Bz.writeFile "data.json" . encode

asBz :: HubGraph -> Bz.ByteString
asBz = encode

load :: IO HubGraph
load = fromMaybe Gr.empty . decode' <$> Bz.readFile "data.json"
