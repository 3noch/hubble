module Hubble.Graph.Types where

import Data.Graph.Inductive (Gr)

type NodeName = String
type EdgeName = String

type HubGraph = Gr NodeName EdgeName
