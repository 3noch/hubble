{-# LANGUAGE FlexibleContexts #-}

module Hubble.Graph.Query.Lang
  ( Depth(..)
  , EdgeLabel(..)
  , Expr(..)
  , parse
  ) where

import Control.Applicative ((<$>), (<*>), (*>), (<*), liftA)
import Data.Char (digitToInt)
import Data.List (foldl')
import Text.Parsec
  ( ParsecT, ParseError, Stream
  , (<?>), (<|>)
  , alphaNum, char, digit, eof, letter, many, many1, option, spaces, string, try)
import qualified Text.Parsec as P
import Text.Parsec.Expr (Assoc(..), Operator(..), buildExpressionParser)
import Text.Parsec.String (Parser)


data Depth = DepthOf !Int | Infinity
             deriving (Show, Eq)

data EdgeLabel = WildEdge | Label !String
                 deriving (Show, Eq)

data Expr = EmptyExpr
          | WildNode
          | NodeId   !Int
          | NodeName !String
          | To   !EdgeLabel !Depth !Expr !Expr
          | From !EdgeLabel !Depth !Expr !Expr
          | And !Expr !Expr
          | Or  !Expr !Expr
          | Capture !Expr
          deriving (Show, Eq)

lexeme :: Parser a -> Parser a
lexeme p = spaces *> p <* spaces

positiveNatural :: Stream s m Char => ParsecT s u m Int
positiveNatural = foldl' (\a i -> a * 10 + digitToInt i) 0 <$> many1 digit
               <?> "non-negative integer"

identifier :: Parser String
identifier = (:) <$> letter <*> many (alphaNum <|> char '_')
          <?> "identifier"

edgeLabel :: Parser EdgeLabel
edgeLabel =   char '_' *> return WildEdge
          <|> Label <$> identifier
          <?> "edge label"

to_ :: Parser (Expr -> Expr -> Expr)
to_ = rightArrow >>= (\(lbl, depth) -> return $ To lbl depth)
  where
    rightArrow = (,) <$> arrowBody <*> tip <?> "right arrow"
      where
        tip = string "*>" *> return Infinity
           <|> DepthOf <$> positiveNatural <* char '>'
           <|> DepthOf <$> liftA length (many1 (char '>'))

from_ :: Parser (Expr -> Expr -> Expr)
from_ = leftArrow >>= (\(depth, lbl) -> return $ From lbl depth)
  where
    leftArrow = (,) <$> tip <*> arrowBody <?> "left arrow"
      where
        tip = char '<' *>
           (  char '*' *> return Infinity
          <|> DepthOf <$> positiveNatural
          <|> DepthOf <$> liftA (succ . length) (many (char '<'))
           )

arrowBody :: Parser EdgeLabel
arrowBody = char '-' *> option WildEdge edgeLabel <* char '-'

and_, or_ :: Parser (Expr -> Expr -> Expr)
and_ = char '&' *> return And
or_  = char '|' *> return Or

surroundedBy :: Char -> Char -> Parser a -> Parser a
surroundedBy left right p = char left *> p <* char right

parens, braces :: Parser a -> Parser a
parens = surroundedBy '(' ')'
braces = surroundedBy '{' '}'

expr :: Parser Expr
expr = buildExpressionParser table term
    <?> "expression"
  where
    table = [ [Infix to_ AssocRight, Infix from_ AssocRight]
            , [Infix and_ AssocLeft]
            , [Infix or_  AssocLeft]
            ]

term :: Parser Expr
term = lexeme
     (  try (parens spaces *> return EmptyExpr)
    <|> parens expr
    <|> char '_' *> return WildNode
    <|> NodeId   <$> positiveNatural
    <|> NodeName <$> identifier
    <|> Capture  <$> braces expr
     ) <?> "term"

parse :: String -> Either ParseError Expr
parse = P.parse (expr <* eof) ""
