{-# LANGUAGE TemplateHaskell #-}

module Hubble.Graph
  ( readJsonGraph
  , graphToJson
  ) where

import           Data.Aeson.TH (deriveJSON, defaultOptions, fieldLabelModifier)
import           Data.Map (Map)
import qualified Data.Map as Map
import           Control.Applicative ((<$>))
import           Data.Graph (Edge, Vertex)

type Node = Vertex

data JsonNode = JsonNode
  { _name :: String
  , _group :: Int }
$(deriveJSON defaultOptions { fieldLabelModifier = drop 1} ''JsonNode)

data JsonEdge = JsonEdge
  { _source :: Int
  , _target :: Int
  , _value  :: Int }
$(deriveJSON defaultOptions { fieldLabelModifier = drop 1} ''JsonEdge)

data JsonGraph = JsonGraph
  { _nodes :: [JsonNode]
  , _links :: [JsonEdge] }
$(deriveJSON defaultOptions { fieldLabelModifier = drop 1} ''JsonGraph)


data LabeledGraph n e = LabeledGraph
  { _nodeLabels :: Map Node n
  , _edgeLabels :: Map Edge e
  }

type MyLabeledGraph = LabeledGraph JsonNode Int


readJsonGraph :: JsonGraph -> MyLabeledGraph
readJsonGraph (JsonGraph nodes links) = LabeledGraph nodes' edges'
  where
    nodes' = Map.fromList (zip [1..] nodes)
    edges' = Map.fromList (convertEdge <$> links)
    convertEdge (JsonEdge src tar val) = ((src, tar), val)


graphToJson :: MyLabeledGraph -> JsonGraph
graphToJson (LabeledGraph nodes links) =
    JsonGraph (Map.elems nodes)
              (convertEdge <$> Map.toList links)
  where
    convertEdge ((src, tar), val) = JsonEdge src tar val
