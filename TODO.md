1. API to provide graph as JSON
    1. Nodes are nameless circles
    2. Nodes show ids
2. Render graph with force-layout
3. Sticky force layout
4. Update positions from sticky force layout from client to server
